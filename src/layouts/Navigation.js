import {Link} from "react-router-dom";


const Navigation = () => {
    const navigations = [
        {
            name: "Home",
            path: "/"
        },
        {
            name: "Done Page",
            path: "/done"
        },
        {
            name: "About Page",
            path: "/about"
        },
        {
            name: "Help Page",
            path: "/help"
        }
    ]

    return (
        <nav className="flex flex-row justify-between text-white bg-blue-400 px-4 py-2">
            <p>Navigation Bar</p>
            <ul className="flex flex-row gap-4">
                {
                    navigations.map((navigation, index) => (
                        <li key={index}>
                            <Link className="hover:underline"
                                  to={navigation.path}>{navigation.name}</Link>
                        </li>
                    ))
                }
            </ul>
        </nav>
    );
}

export default Navigation;