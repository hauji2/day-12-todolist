import {Outlet} from "react-router-dom";
import Navigation from "./Navigation";

const Layout = () => {
    return (
        <div className="flex flex-col gap-2">
            <Navigation/>
            <Outlet/>
        </div>
    );
}

export default Layout;