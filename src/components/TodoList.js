import TodoGroup from "./TodoGroup";
import TodoGenerator from "./TodoGenerator";
import {useEffect} from "react";
import {useDispatch} from "react-redux";
import useTodos from "../hooks/useTodos";

const TodoList = () => {
    const dispatch = useDispatch();
    const {loadTodosHook} = useTodos();

    useEffect(() => {
        loadTodosHook();
    }, [loadTodosHook, dispatch]);

    return (
        <div className="flex flex-col gap-14 w-auto items-center">
            <h1 className="w-fit text-3xl font-bold italic hover:underline hover:font-mono hover:bg-amber-600 rounded">
                TodoList
            </h1>
            <TodoGroup/>
            <TodoGenerator/>
        </div>
    )
}

export default TodoList