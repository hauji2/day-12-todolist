import useTodos from "../hooks/useTodos";
import toast from "react-hot-toast";
import {useState} from "react";
import {Button, Input} from "antd";

const TodoGenerator = () => {
    const {addTodoHook} = useTodos();
    const [text, setText] = useState("");

    const handleSubmit = (event) => {
        if (text.trim() === '') {
            toast.error("Todo cannot be empty");
            return;
        }

        addTodoHook({text: text});
        setText("");
    }

    return (
        <div className="flex flex-row justify-center items-center gap-4">
            <Input className="border border-black w-72 rounded"
                   placeholder="input a new todo here..."
                   value={text}
                   onChange={(e) => setText(e.target.value)}
                   onPressEnter={handleSubmit}/>
            <Button
                shape="circle"
                type="default"
                className="bg-blue-200 hover:bg-blue-300 font-bold"
                onClick={handleSubmit}
                icon={"+"}
            />
        </div>
    )
}

export default TodoGenerator