import useTodos from "../hooks/useTodos";
import {Divider, Input, Modal} from "antd";
import {useEffect, useState} from "react";

const TodoItem = ({item}) => {
    const {updateTodoHook, deleteTodoHook} = useTodos();
    const [showModel, setShowModel] = useState(false);
    const [text, setText] = useState(item.text);
    const [isTextValid, setIsTextValid] = useState(true);

    useEffect(() => {
        setText(item.text);
    }, [item.text]);


    const handleDeleteClick = async () => {
        const isConfirmed = window.confirm("Are you sure you want to delete this item?");

        if (isConfirmed) {
            deleteTodoHook(item.id);
        }
    }

    const handleItemClick = () => {
        updateTodoHook({...item, done: !item.done});
    }

    const handleEditClick = () => {
        setShowModel(true);
    }

    const handleClickOk = () => {
        updateTodoHook({...item, text: text});
        handleCancel();
    }

    const handleCancel = () => {
        setShowModel(false);
        setText(item.text);
    }

    const isTextValidHandler = (e) => {
        setIsTextValid(text.trim() !== "");
    }

    return (
        <div>
            <div
                className={`flex flex-row gap-3 border border-gray-400 w-96 px-3 rounded justify-between 
            ${item.done && "line-through"} hover:bg-pink-200 hover:border-2`}>
                <p onClick={handleItemClick}>{item.text}</p>
                <div className="flex flex-row gap-3">
                    <button onClick={handleDeleteClick}>
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5"
                             stroke="currentColor" className="w-6 h-6">
                            <path strokeLinecap="round" strokeLinejoin="round" d="M6 18L18 6M6 6l12 12"/>
                        </svg>
                    </button>
                    <button onClick={handleEditClick}>
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                             className="bi bi-pencil" viewBox="0 0 16 16">
                            <path
                                d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z"/>
                        </svg>
                    </button>
                </div>
            </div>
            <Modal title="ToDoItem"
                   open={showModel}
                   onOk={handleClickOk}
                   onCancel={handleCancel}
                   okButtonProps={{className: "bg-green-500 text-black rounded", disabled: !isTextValid}}
                   cancelButtonProps={{className: "bg-red-400 text-black rounded"}}
            >
                <Divider className="mt-0"/>
                <Input.TextArea className={`${!isTextValid && "border-red-400"}`} value={text}
                                onChange={(e) => {
                                    setText(e.target.value);
                                    isTextValidHandler();
                                }}
                                onBlur={isTextValidHandler}/>
                <Divider className="mb-0"/>
            </Modal>
        </div>
    )
}

export default TodoItem