import {useSelector} from "react-redux";
import TodoItem from "./TodoItem";

const TodoGroup = () => {
    const items = useSelector((state) => state.todoList.items);
    return (
        <div className="flex flex-col gap-2 items-center overflow-y-auto max-h-96">
            {items.map((item) => (
                <TodoItem key={item.id} item={item}/>
            ))}
        </div>
    )

}

export default TodoGroup