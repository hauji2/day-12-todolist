import './App.css';
import {RouterProvider} from "react-router-dom";
import React from "react";
import router from "./routers/router";
import {Toaster} from "react-hot-toast";

function App() {
    return (
        <div className="App">
            <Toaster/>
            <RouterProvider router={router}/>
        </div>
    );
}

export default App;
