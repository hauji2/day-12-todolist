import {createSlice} from '@reduxjs/toolkit'

export const todoListSlice = createSlice({
    name: 'todoList',
    initialState: {
        items: [],
    },
    reducers: {
        reloadItems: (state, action) => {
            state.items = action.payload
        }
    },
})

export const {reloadItems} = todoListSlice.actions

export default todoListSlice.reducer