import client from "./client";

const getTodos = () => {
    return client.get("/todoList").then((response) => {
        return response.data
    }).catch(error => {
        return error;
    })
}

const addTodo = (todo) => {
    return client.post("/todoList", todo).then((response) => {
        return response.data
    }).catch(error => {
        return error;
    })
}

const updateTodo = (todo) => {
    return client.put(`/todoList/${todo.id}`, todo).then((response) => {
        return response.data
    }).catch(error => {
        return error;
    })
}

const deleteTodo = (id) => {
    return client.delete(`/todoList/${id}`).then((response) => {
        return response.data
    }).catch(error => {
        return error;
    })
}

export {
    getTodos,
    addTodo,
    updateTodo,
    deleteTodo
}