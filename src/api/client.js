import axios from "axios";

const client = axios.create({
    baseURL: 'https://6565ec29eb8bb4b70ef29742.mockapi.io/api/',
})

export default client;