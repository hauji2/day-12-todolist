import {useDispatch} from "react-redux";
import {reloadItems} from "../app/todoListSlice";
import {addTodo, deleteTodo, getTodos, updateTodo} from "../api/todos";
import toast from "react-hot-toast";

const useTodos = () => {
    const dispatch = useDispatch();
    const loadTodosHook = () => {
        getTodos().then((response) => {
            dispatch(reloadItems(response));
        })
    }

    const addTodoHook = (text) => {
        addTodo(text).then(() => {
            toast.success("Todo added");
            loadTodosHook();
        })
            .catch(err => {
                console.log(err);
                toast.error("Todo not added, there was an error");
            });
    }

    const updateTodoHook = (todo) => {
        updateTodo(todo).then(() => {
            loadTodosHook();
            toast.success("Todo updated");
        })
            .catch(err => {
                console.log(err);
                toast.error("Todo not updated, there was an error");
            });
    }

    const deleteTodoHook = (id) => {
        deleteTodo(id).then(() => {
            toast.success("Todo deleted");
            loadTodosHook();
        })
            .catch(err => {
                console.log(err);
                toast.error("Todo not deleted, there was an error");
            });
    }

    return {
        loadTodosHook,
        addTodoHook,
        updateTodoHook,
        deleteTodoHook
    }
}

export default useTodos;