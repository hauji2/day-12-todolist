import {createBrowserRouter} from "react-router-dom";
import TodoList from "../components/TodoList";
import ErrorPage from "../pages/ErrorPage";
import React from "react";
import AboutPage from "../pages/AboutPage";
import HelpPage from "../pages/HelpPage";
import DonePage from "../pages/DonePage";
import Layout from "../layouts/Layout";
import ToDoDetailPage from "../pages/ToDoDetailPage";

const router = createBrowserRouter([
    {
        path: "/",
        element: <Layout/>,
        errorElement: <ErrorPage/>,
        children: [
            {
                index: true,
                element: <TodoList/>
            },
            {
                path: "about",
                element: <AboutPage/>
            },
            {
                path: "help",
                element: <HelpPage/>
            },
            {
                path: "done",
                element: <DonePage/>
            },
            {
                path: "/todo/:id",
                element: <ToDoDetailPage/>
            }
        ]
    },

]);

export default router;